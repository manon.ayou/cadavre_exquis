# cadavre_exquis

    Un jeu de cadavre exquis  en python, pour une première prise en main.

###
Installation de Python via le terminal Linux:

    sudo apt-get install python3

###
Installation des packages :

    sudo apt install python3-pip

###
Pour tester le programme, ouvrir le fichier dans le terminal.
    cd chemin
    python3 <nom_du_fichier>